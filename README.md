# Jammy Dodger

## A kids 2d arcade speed run game 



### Issues
#### Browser issue
An issue can occur when using Chrome where you see an error saying   
'Uncaught (in promise) DOMException' when using the '.play()' function. 

If this happens copy and paste the following URL into your Chrome browser 
chrome://flags/#autoplay-policy and in the 'Autoplay policy' section in  
the dropdown box change it to 'No user gesture is required.'. 



### References
#### Jammy Dodger jumping sound
* [opengameart.org jumping sounds](https://opengameart.org/content/platformer-jumping-sounds "Opengameart.org jumping sounds")

#### Jammy Dodger character reference
* [www.shutterstock.com/image-vector](https://www.shutterstock.com/image-vector/superhero-cartoon-jam-glass-jar-character-1182020491 "www.shutterstock.com/image-vector")

#### Music
* [Free music archive](http://freemusicarchive.org/genre/Chiptune/ "Free music archive")


