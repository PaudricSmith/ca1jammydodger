window.onload = function () {


    var requestAnimationFrame = (function () {
        return window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.oRequestAnimationFrame ||
            window.msRequestAnimationFrame ||
            function (callback, element) {
                window.setTimeout(callback, 1000 / 60);
            };
    })();




    // Variables //////////////////////////////////////////////////////////////////
    var canvas = document.getElementById('game');
    var context = canvas.getContext('2d');
    var fps = document.getElementById('fps');
    var theme = document.getElementById('theme');
    var introMusic = document.getElementById('introMusic');
    var goalMusic = document.getElementById('goalMusic');
    var gameOverMusic = document.getElementById('gameOverMusic');

    var jumpSound = document.getElementById('jumpSound');
    var levelSound = document.getElementById('levelSound');
    var victorySound = document.getElementById('victorySound');
    var defeatSound = document.getElementById('defeatSound');

    var keys = [];
    var friction = 0.5;
    var gravity = 0.98;
    var startTime = Date.now();
    var frame = 0;
    var milliSeconds = 0;
    var seconds = 0;

    var nameNotEntered = true;
    var completed = false;
    var reseted = false;
    var gameStarted = false;
    var countDownFinished = false;
    var paused = false;
    var musicOn = true;
    var inIntroScreen = true;
    var inHighScoreScreen = false;
    var inLevelScreen = false;
    var level1Completed = false;
    var collectableCount = 0;
    var spoonCollisionCount = 0;
    var targeted1 = false;
    var targeted2 = false;
    var targeted3 = false;
    var isGameOver = false;

    var zero = 0;
    var highscore = 400;
    
    
    highscore = localStorage.getItem("highscore");
    

    localStorage.setItem("playerName2", "bbbbb");
    localStorage.setItem("playerName3", "ccccc");
    localStorage.setItem("playerName4", "ddddd");
    localStorage.setItem("playerName5", "eeeee");

    localStorage.setItem("highscore2", 400);
    localStorage.setItem("highscore3", 300);
    localStorage.setItem("highscore4", 200);
    localStorage.setItem("highscore5", 100);


    console.log(localStorage.getItem("highscore"));
    console.log(highscore);


    const playerArray = [[localStorage.getItem("playerName"), localStorage.getItem("highscore")],
    [localStorage.getItem("playerName2"), localStorage.getItem("highscore2")],
    [localStorage.getItem("playerName3"), localStorage.getItem("highscore3")],
    [localStorage.getItem("playerName4"), localStorage.getItem("highscore4")],
    [localStorage.getItem("playerName5"), localStorage.getItem("highscore5")]];





    //function compare(a, b)
    //{
    //    return b.score - a.score;
    //}
    //
    //playerArray.sort(compare());




    //spoon enemy spritesheet ////////////////////////////////////////////
    SpoonSpritesheet = new SpriteSheet('images/SpoonSpriteSheet.png', 24, 100);
    var spoonAnimation = new Animation(SpoonSpritesheet, 12, 0, 1);

    //strawberry flashing spritesheet ////////////////////////////////////////////
    StrawberrySpritesheet = new SpriteSheet('images/strawberrySpriteSheet.png', 64, 64);
    var strawberryAnimation = new Animation(StrawberrySpritesheet, 8, 0, 7);

    //Jammy Dodger animations ///////////////////////////////////////////////
    JamDodgerMainSpritesheet = new SpriteSheet('images/JammyDodgerMainSpriteSheet.png', 100, 90);
    JamDodgerFlexSpritesheet = new SpriteSheet('images/JammyDodgerFlexSpriteSheet.png', 109, 95);
    JamDodgerFlossSpritesheet = new SpriteSheet('images/JammyDodgerFlossDance.png', 130, 100);

    var runRight = new Animation(JamDodgerMainSpritesheet, 11, 4, 7);
    var runLeft = new Animation(JamDodgerMainSpritesheet, 11, 8, 11);
    var standingCenter = new Animation(JamDodgerFlexSpritesheet, 8, 0, 11);
    var flossing = new Animation(JamDodgerFlossSpritesheet, 7, 0, 7);


    // Jammy Dodger Logo animation ///////////////////////////////////////////////
    spritesheetLogo = new SpriteSheet('images/JammyDodgerLogoAnimation.png', 198, 93);
    var jammyDodgerLogoAnimation = new Animation(spritesheetLogo, 5, 0, 30);




    // SpriteSheet function /////////////////////////////////////////////////////
    function SpriteSheet(path, frameWidth, frameHeight) {
        this.image = new Image();
        this.frameWidth = frameWidth;
        this.frameHeight = frameHeight;

        // calculates the number of frames in a row after the image loads
        var self = this;
        this.image.onload = function () {
            self.framesPerRow = Math.floor(self.image.width / self.frameWidth);
        };

        this.image.src = path;
    }




    // Animation function ///////////////////////////////////////////////////////
    function Animation(spritesheet, frameSpeed, startFrame, endFrame) {

        var animationArray = [];  // array holding the animation
        var currentFrame = 0;        // the current frame to draw
        var counter = 0;             // keeps track of frame rate

        // creates the sequence of frame numbers for the animation
        for (var frameNumber = startFrame; frameNumber <= endFrame; frameNumber++)
            animationArray.push(frameNumber);

        // Updates the animation
        this.update = function () {

            // updates to the next frame if it is time
            if (counter === (frameSpeed - 1))
                currentFrame = (currentFrame + 1) % animationArray.length;

            // updates the counter
            counter = (counter + 1) % frameSpeed;
        };

        // draw the current frame
        this.draw = function (x, y) {
            // gets the row and col of the frame
            var row = Math.floor(animationArray[currentFrame] / spritesheet.framesPerRow);
            var col = Math.floor(animationArray[currentFrame] % spritesheet.framesPerRow);

            context.drawImage(
                spritesheet.image,
                col * spritesheet.frameWidth, row * spritesheet.frameHeight,
                spritesheet.frameWidth, spritesheet.frameHeight,
                x, y,
                spritesheet.frameWidth, spritesheet.frameHeight);
        };
    }







    // Screen fps ///////////////////////////////////////////////////////////////////
    function fpsCounter() {
        var time = Date.now();

        frame++;
        if (time - startTime > 1000) {
            fps.innerHTML = (frame / ((time - startTime) / 1000)).toFixed(0) + " fps";
            startTime = time;
            frame = 0;
        }
        window.requestAnimationFrame(fpsCounter);
    }
    fpsCounter();











    // player object ////////////////////////////////////////
    var player = {
        x: canvas.width / 8,
        y: canvas.height - canvas.height / 8,
        width: 80,
        height: 80,
        speed: 5,
        velX: 0,
        velY: 0,

        name: "not-entered",
        jumping: false,
        grounded: false,
        jumpStrength: 7,
        position: "idle",
        isAlive: true,
        time: 0,
        score: 0,
        totalScore: 0,

        drawLeft: function () {
            runLeft.draw(this.x, this.y);
        },
        drawRight: function () {
            runRight.draw(this.x, this.y);
        },
        drawStandingCenter: function () {
            standingCenter.draw(this.x, this.y);
        },
        drawFloss: function () {
            flossing.draw(350, 250);
        }
    };




    // enemy spoons ///////////////////////////////////////////////////////////////////////////////
    var spoon1 = {
        x: Math.floor((Math.random() * 700) + 50),
        y: -100,
        width: 50,
        height: 50,
        draw: function () {
            spoonAnimation.draw(this.x, this.y);
        }
    }

    var spoon2 = {
        x: Math.floor((Math.random() * 700) + 50),
        y: -100,
        width: 50,
        height: 50,
        draw: function () {
            spoonAnimation.draw(this.x, this.y);
        }
    }

    var spoon3 = {
        x: Math.floor((Math.random() * 700) + 50),
        y: -100,
        width: 50,
        height: 50,
        draw: function () {
            spoonAnimation.draw(this.x, this.y);
        }
    }




    // collectable strawberry's ///////////////////////////////////////////////////////////////////////////////
    var collectable1 = {
        x: 450,
        y: 455,
        width: 50,
        height: 50,
        draw: function () {
            strawberryAnimation.draw(this.x, this.y);
        }
    }

    var collectable2 = {
        x: 10,
        y: 340,
        width: 50,
        height: 50,
        draw: function () {
            strawberryAnimation.draw(this.x, this.y);
        }
    }

    var collectable3 = {
        x: 600,
        y: 50,
        width: 50,
        height: 50,
        draw: function () {
            strawberryAnimation.draw(this.x, this.y);
        }
    }

    var collectable4 = {
        x: canvas.width - 670,
        y: 50,
        width: 50,
        height: 50,
        draw: function () {
            strawberryAnimation.draw(this.x, this.y);
        }
    }




    // Platforms //////////////////////////////
    var platforms = [];
    var platform_width = 120;
    var platform_height = 10;

    // #1
    platforms.push({
        x: canvas.width - 170,
        y: canvas.height - 50,
        width: platform_width,
        height: platform_height
    });
    // #2
    platforms.push({
        x: canvas.width - 380,
        y: canvas.height - 120,
        width: platform_width,
        height: platform_height
    });
    // #3
    platforms.push({
        x: canvas.width - 590,
        y: canvas.height - 180,
        width: platform_width,
        height: platform_height
    });
    // #4
    platforms.push({
        x: canvas.width - 370,
        y: canvas.height - 260,
        width: platform_width,
        height: platform_height
    });
    // #5 
    platforms.push({
        x: canvas.width - 170,
        y: canvas.height - 330,
        width: platform_width,
        height: platform_height
    });
    // #6
    platforms.push({
        x: canvas.width - 400,
        y: canvas.height - 420,
        width: platform_width,
        height: platform_height
    });
    // #7
    platforms.push({
        x: canvas.width - 700,
        y: canvas.height - 320,
        width: platform_width,
        height: platform_height
    });
    // #8
    platforms.push({
        x: canvas.width - 800,
        y: canvas.height - 420,
        width: platform_width,
        height: platform_height
    });
    // #9
    platforms.push({
        x: canvas.width - 700,
        y: canvas.height - 520,
        width: platform_width,
        height: platform_height
    });

    // Boundaries /////////////////////////
    // ceiling
    platforms.push({
        x: 0,
        y: -70,
        width: canvas.width,
        height: platform_height
    });

    // Floor
    platforms.push({
        x: 0,
        y: canvas.height - 10,
        width: canvas.width,
        height: platform_height
    });

    // Left Wall
    platforms.push({
        x: -10,
        y: 0,
        width: 10,
        height: canvas.height
    });

    // Right Wall
    platforms.push({
        x: canvas.width,
        y: 0,
        width: 10,
        height: canvas.height
    });




    // Event Listeners ////////////////////////////////////////////////
    document.body.addEventListener("keydown", function (event) {
        console.log('In event listener');



        // when 'enter' is pressed game level screen didn't show yet //////////////////////
        if (event.keyCode === 13 && !inLevelScreen) {
            console.log('Inside Enter key pressed and !inLevelScreen === false');
            inLevelScreen = true;
            inIntroScreen = false;
            inHighScoreScreen = false;
            levelScreen();
        }

        // when 'enter' is pressed game didn't start yet //////////////////////
        else if (event.keyCode === 13 && !gameStarted) {
            console.log('Inside Enter key pressed and gameStarted === false');
            startGame();
        }

        // when 'enter' is pressed and game is completed ///////////////////////
        if (event.keyCode === 13 && completed && player.isAlive) {
            console.log('Inside Enter key pressed and completed === true');
            if (level1Completed) {
                level2();
            }
            else {
                reset();
            }
        }

        // when 'enter' is pressed and player is dead /////////////
        if (event.keyCode === 13 && completed && !player.isAlive) {
            reset();
        }

        // when 'h' is pressed /////////////////////////////////////////////////
        if (event.keyCode === 72 && !gameStarted && inIntroScreen) {
            if (!inHighScoreScreen) {
                console.log('Inside h pressed');
                inIntroScreen = false;
                inHighScoreScreen = true;
                highScoreScreen();
            }
        }

        // when 'b' is pressed in the highscore screen /////////////////////////////////////////////////
        if (event.keyCode === 66 && !gameStarted && inHighScoreScreen) {
            if (!inIntroScreen) {
                console.log('Inside b pressed');
                inIntroScreen = true;
                inHighScoreScreen = false;
                introScreen();
            }
        }

        // when 'b' is pressed in the level 1 screen ///////////////////////////////////////////////////
        else if (event.keyCode === 66 && !gameStarted && inLevelScreen) {
            if (!inIntroScreen) {
                console.log('Inside b pressed');
                inIntroScreen = true;
                inLevelScreen = false;
                introScreen();
            }
        }

        // when 'p' is pressed and in middle of game //////////////////////////
        if (event.keyCode === 80 && !completed && gameStarted) {
            console.log('Inside pause listener');
            console.log(paused);
            if (paused) {
                musicOn = true;
                paused = false;
                gameLoop();
                console.log(paused);
            }
            else if (!paused) {

                musicOn = false;
                paused = true;
                console.log(paused);
            }


        }

        // toggle music //////////////////////////////////////
        if (event.keyCode === 77) {
            console.log('Inside music toggle listener');

            if (musicOn) {
                musicOn = false;
                console.log(musicOn);
            }
            else if (!musicOn) {
                musicOn = true;
                console.log(musicOn);
            }

        }

        keys[event.keyCode] = true;

    });

    document.body.addEventListener("keyup", function (event) {
        keys[event.keyCode] = false;
    });




    // Intro Screen //////////////////////////////////////////////////////////////////////////
    introScreen();

    function introScreen() {

        if (nameNotEntered) {
            player.name = prompt('What is your name?');
            nameNotEntered = false;
        }

        console.log('Inside introScreen');
        console.log(highscore);
        console.log(localStorage.getItem("highscore"));

        if (musicOn) {
            introMusic.play();
        }
        else if (!musicOn) {
            introMusic.pause();
            levelSound.pause();
        }

        context.fillStyle = "#000000";
        context.fillRect(0, 0, canvas.width, canvas.height);
        context.font = "50px Arial";
        context.fillStyle = "#b62e2e";
        context.textAlign = "center";

        jammyDodgerLogoAnimation.draw(300, 170);
        jammyDodgerLogoAnimation.update();


        // #6599FF - neon blue
        // #03CDFF - light neon blue
        context.fillStyle = "#03CDFF";
        context.font = "20px Arial";
        context.fillText("Keyboard:", canvas.width / 2, canvas.height / 2 - 40);
        context.font = "15px Arial";
        context.fillText("Move left / right - left / right arrow keys", canvas.width / 2, canvas.height / 2 + 10);
        context.fillText("Jump - up arrow key / Space bar", canvas.width / 2, canvas.height / 2 + 40);
        context.fillText("P - pause game", canvas.width / 2, canvas.height / 2 + 60);
        context.fillText("M - mute music", canvas.width / 2, canvas.height / 2 + 80);
        context.fillText("H - highscores", canvas.width / 2, canvas.height / 2 + 100);
        context.fillText("B - back to intro screen from highscores", canvas.width / 2, canvas.height / 2 + 120);
        context.font = "20px Arial";
        context.fillText("Press Enter To Start", canvas.width / 2, canvas.height - 70);

        if (!gameStarted && !inLevelScreen && !inHighScoreScreen) {
            requestAnimationFrame(introScreen);
        }
        else {
            introMusic.pause();
            introMusic.currentTime = 0;
        }
    }

    function levelScreen() {

        if (nameNotEntered) {
            player.name = prompt('What is your name?');
            nameNotEntered = false;
        }

        console.log('Inside levelScreen');
        console.log(highscore);
        console.log(localStorage.getItem("highscore"));

        introMusic.pause();

        if (musicOn) {
            levelSound.play();
        }
        else if (!musicOn) {
            levelSound.pause();
        }

        context.fillStyle = "#000000";
        context.fillRect(0, 0, canvas.width, canvas.height);
        context.font = "50px Arial";
        context.fillStyle = "#b62e2e";
        context.textAlign = "center";

        jammyDodgerLogoAnimation.draw(300, 170);
        jammyDodgerLogoAnimation.update();

        context.fillStyle = "#03CDFF";
        // #6599FF - neon blue
        // #03CDFF - light neon blue
        context.font = "30px Arial";
        context.fillText("Level 1", canvas.width / 2, canvas.height / 2 - 60);
        context.fillStyle = "#03CDFF";
        context.font = "20px Arial";
        context.fillText("Eat all the strawberrys in the sky as quick as you can!", canvas.width / 2, canvas.height / 2 + 10);
        context.font = "15px Arial";
        context.fillText("Under 12 seconds = 3 Floss Trophy", canvas.width / 2, canvas.height / 2 + 60);
        context.fillText("Under 15 seconds = 2 Floss Trophy", canvas.width / 2, canvas.height / 2 + 80);
        context.fillText("Under 18 seconds = 1 Floss Trophy", canvas.width / 2, canvas.height / 2 + 100);
        context.font = "20px Arial";
        context.fillText("Press Enter To Start", canvas.width / 2, canvas.height - 70);

        if (!gameStarted && inLevelScreen) {
            requestAnimationFrame(levelScreen);
        }
        else {
            levelSound.pause();
            levelSound.currentTime = 0;
        }
    }




    // High score screen///////////////////////////////////////////////////////////////////////
    function highScoreScreen() {

        console.log('Inside highScoreScreen');

        if (musicOn) {
            introMusic.play();
        }
        else if (!musicOn) {
            introMusic.pause();
        }

        context.fillStyle = "#000000";
        context.fillRect(0, 0, canvas.width, canvas.height);
        context.font = "50px Impact";
        context.fillStyle = "#03CDFF";
        context.textAlign = "center";

        context.fillText("***** HIGHSCORES *****", canvas.width / 2, 200);
        context.font = "30px Impact";

        displayArray(playerArray);

        if (!gameStarted && !inIntroScreen) {
            requestAnimationFrame(highScoreScreen);
        }
        else {
            introMusic.pause();
            introMusic.currentTime = 0;
        }
    }




    function startGame() {
        console.log('In startGame function');

        gameStarted = true;
        countDownFinished = false;

        requestAnimationFrame(gameLoop);
    }




    function complete() {

        console.log('Inside complete function');

        clearCanvas();
        completed = true;

        goalMusic.volume = 0.4;
        if (musicOn) {
            if (theme.play()) {
                theme.pause();
                theme.currentTime = 0;
            }
            if (!goalMusic.play()) {

                goalMusic.play();
            }
        }
        else if (!musicOn) {
            goalMusic.pause();
        }


        context.fillStyle = "#000000";
        context.fillRect(0, 0, canvas.width, canvas.height);
        context.fillStyle = "#03CDFF";
        context.fillRect(10, 432, canvas.width - 20, 120);
        context.fillRect(10, 190, canvas.width - 20, 170);
        context.fillStyle = "#6599FF";
        context.lineWidth = 3;
        context.strokeRect(15, 195, canvas.width - 30, 160);
        draw3Stars();



        let flossTrophy = 1000;
        let trophyCount = 0;
        let trophyScore = 0;

        if (player.time < 12) {
            trophyCount = 3;
            flossing.draw(canvas.width / 4 - 65, 442);
            flossing.draw(canvas.width / 2 - 65, 442);
            flossing.draw(canvas.width - canvas.width / 4 - 65, 442);
            flossing.update();
        }
        else if (player.time < 15) {
            trophyCount = 2;
            flossing.draw(canvas.width / 4 - 65, 442);
            flossing.draw(canvas.width / 2 - 65, 442);
            flossing.update();
        }
        else if (player.time < 18) {
            trophyCount = 1;
            flossing.draw(canvas.width / 4 - 65, 442);
            flossing.update();
        }
        else {
            trophyCount = 0;
        }

        context.lineWidth = 3;
        context.strokeRect(15, 437, canvas.width - 30, 110);
        context.font = "30px Impact";
        context.fillStyle = "#b62e2e";
        context.textAlign = "center";


        // Score calculations /////////////////////////////////////////////////////////////////
        player.score = (10000 / player.time).toFixed(0);
        trophyScore = (flossTrophy * trophyCount);
        player.totalScore = ((10000 / player.time) + trophyScore).toFixed(0);
        
        localStorage.setItem("highscore", highscore);
        highscore = localStorage.getItem("highscore");

        console.log(player.totalScore);
        console.log(localStorage.getItem("highscore"));
        console.log(highscore);

        if (player.totalScore > highscore) {
            console.log("inside highscore if");
            localStorage.setItem("highscore", player.totalScore);
            localStorage.setItem("playerName", player.name);
        }

        highscore = localStorage.getItem("highscore");

        context.fillStyle = "#6599FF";
        context.font = "50px Impact";
        context.textAlign = "center";
        context.fillText("Results", canvas.width / 2, canvas.height / 2 - 220);
        context.font = "30px Impact";
        context.fillText("Level 1 Completed!", canvas.width / 2, canvas.height / 2 - 150);

        context.fillStyle = "#b62e2e";


        if (player.totalScore === highscore) {
            context.fillStyle = "#b62e2e";
            context.fillText("Weldone " + player.name + " A new High Score!", canvas.width / 2, canvas.height / 2 - 185);
        }

        context.fillText("Time     : " + " " + player.time + " seconds!", canvas.width / 2, canvas.height / 2 - 100);
        context.fillText("Trophies : " + " " + trophyCount + " x " + flossTrophy + " points!", canvas.width / 2, canvas.height / 2 - 60);
        context.fillText("Score    : " + " " + player.score + " points!", canvas.width / 2, canvas.height / 2 - 20);
        context.fillText("Total    : " + " " + player.totalScore + " points!", canvas.width / 2, canvas.height / 2 + 20);

        context.font = "40px Impact";
        context.fillStyle = "#6599FF";
        context.fillText("Trophies", canvas.width / 2, canvas.height / 2 + 90);

        context.font = "20px Arial";
        context.fillText("Press Enter to Play Again", canvas.width / 2, canvas.height - 70);


        console.log(localStorage.getItem("highscore"));
        console.log(highscore);


        if (!reseted) {
            requestAnimationFrame(complete);
        }
        else if (reseted) {
            requestAnimationFrame(reset);
        }

    }


    // level 2 /////////////////////////////////
    function level2() {
        console.log('In reset function');
        console.log(reseted);

        if (goalMusic.play()) {
            goalMusic.pause();
            goalMusic.currentTime = 0;
        }

        player.x = canvas.width / 8;
        player.y = canvas.height - canvas.height / 8;
        player.grounded = true;
        player.velY = 0;
        player.velX = 0;
        completed = false;
        reseted = true;
        milliSeconds = 0;
        countDownFinished = false;
        collectableCount = 0;
        spoonCollisionCount = 0;

        console.log(reseted);

        requestAnimationFrame(gameLoop);
    }




    // resets the game /////////////////////////////////
    function reset() {
        console.log('In reset function');
        console.log(reseted);

        if (musicOn) {
            if (goalMusic.play() || gameOverMusic.play()) {
                goalMusic.pause();
                goalMusic.currentTime = 0;
                gameOverMusic.pause();
                gameOverMusic.currentTime = 0;
            }
        }
        else {
            theme.pause();
            gameOverMusic.pause();
        }


        player.x = canvas.width / 8;
        player.y = canvas.height - canvas.height / 8;
        player.grounded = true;
        player.velY = 0;
        player.velX = 0;

        collectable1.x = 450;
        collectable1.y = 455;
        collectable2.x = 10;
        collectable2.y = 340;
        collectable3.x = 600;
        collectable3.y = 50;
        collectable4.x = canvas.width - 670;
        collectable4.y = 50;

        spoon1.x = Math.floor((Math.random() * 700) + 50);
        spoon1.y = -100;
        spoon2.x = Math.floor((Math.random() * 700) + 50);
        spoon2.y = -100;
        spoon3.x = Math.floor((Math.random() * 700) + 50);
        spoon3.y = -100;

        completed = false;
        reseted = true;
        milliSeconds = 0;
        countDownFinished = false;
        collectableCount = 0;
        spoonCollisionCount = 0;
        player.isAlive = true;

        console.log(reseted);

        requestAnimationFrame(gameLoop);
    }




    // draw functions //////////////////////////////////////////////////////////////////////////
    function drawPlatforms() {
        context.fillStyle = "grey";
        for (var i = 0; i < platforms.length; i++) {
            context.fillRect(platforms[i].x, platforms[i].y, platforms[i].width, platforms[i].height);
            context.lineWidth = 5;
            context.strokeStyle = "white";
            context.strokeRect(platforms[i].x, platforms[i].y - 2, platforms[i].width, 5);
        }
    }



    // displays the time it takes for you to get to the end ///////////////////////////////////////
    function drawTimer() {
        if (seconds > 3) {
            seconds === 0;
            context.fillStyle = "#6abc30";
            context.font = "30px Impact";
            context.fillText((seconds - 3).toFixed(2), canvas.width - 100, 50);
        }
    }



    // displays the 3, 2, 1, Go, countdown at the start of the level ///////////////////////////
    function levelCountdown() {
        if (countDownFinished === false) {
            context.fillStyle = "#62ad2e";
            context.font = "50px Impact";

            if (seconds === 3) {
                countDownFinished = true;
            }
            else if (seconds >= 0 && seconds < 1) {
                context.fillText(3, canvas.width / 2, canvas.height / 2);
            }
            else if (seconds >= 1 && seconds < 2) {
                context.fillText(2, canvas.width / 2, canvas.height / 2);
                if (!theme.play()) {
                    theme.play();
                }
            }
            else if (seconds >= 2 && seconds < 3) {
                context.fillText(1, canvas.width / 2, canvas.height / 2);
            }

        }
    }




    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Main game loop //////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    function gameLoop() {

        inIntroScreen = false;
        inHighScoreScreen = false;
        nameNotEntered = false;

        console.log('In loop function');
        clearCanvas();


        if (paused) {
            drawCircle(canvas.width / 2, canvas.height / 2 - 25, 125, 0, 2 * Math.PI);
            context.fillStyle = "#62ad2e";
            context.font = "70px Impact";
            context.fillText('Paused!', canvas.width / 2, canvas.height / 2);
        }

        reseted = false;
        console.log(reseted);


        // Increments the milliSeconds passed in game /////////		
        milliSeconds++;
        seconds = milliSeconds / 60;



        // displays the go sign at start of game //////////////////////////////
        if (seconds >= 3 && seconds < 4) {
            drawCircle(canvas.width / 2, canvas.height / 2 - 25, 60, 0, 2 * Math.PI);
            context.fillStyle = "#62ad2e";
            context.font = "70px Impact";
            context.fillText('GO!', canvas.width / 2, canvas.height / 2);
        }



        // draw everything //////////
        drawTimer();
        drawPlatforms();
        levelCountdown();

        strawberryAnimation.update();
        spoonAnimation.update();




        // Key controls //////////////////////////////////////////////
        player.position = "idle";

        if (countDownFinished === true) {
            player.time = (seconds - 3).toFixed(2);


            checkSpoonCollisions();

            if (musicOn) {
                // Checks every frame if the main audio 'theme' has finished
                //	playing then play it again /////////////////////////////
                if (!theme.play()) {
                    theme.play();
                }
            }
            else if (!musicOn) {
                theme.pause();
            }



            if (keys[38] || keys[32]) {
                if (!player.jumping) {
                    jumpSound.play();
                    player.velY = -player.jumpStrength * 2;
                    player.jumping = true;
                    player.position = "idle";
                }
            }

            if (keys[39] || (keys[39] && player.jumping === true)) {
                player.position = "right";
                if (player.velX < player.speed) {
                    player.velX += 2;
                    player.drawRight();
                    runRight.update();
                }
            }
            else if (keys[37] || (keys[37] && player.jumping === true)) {
                player.position = "left";
                if (player.velX > -player.speed) {
                    player.velX -= 2;
                    player.drawLeft();
                    runLeft.update();
                }
            }
            else {
                player.drawStandingCenter();
                standingCenter.update();
            }
        }
        else {
            player.drawStandingCenter();
            standingCenter.update();

        }

        ///////////////////////////////////////////////////////////////
        player.x += player.velX;
        player.y += player.velY;

        player.velX *= friction;
        player.velY += gravity;

        player.grounded = false;

        for (var i = 0; i < platforms.length; i++) {
            var direction = collisionCheck(player, platforms[i]);

            if (direction === "left" || direction === "right") {
                player.velX = 0;
            }
            else if (direction === "bottom") {
                player.jumping = false;
                player.grounded = true;
            }
            else if (direction === "top") {
                player.velY *= -1;
            }

        }



        checkCollectableCollisions();



        if (player.grounded) {
            player.velY = 0;
        }

        /* if (collisionCheck(player, strawberry)) {
            
            complete();
        } */

        if (!completed && !paused) {
            requestAnimationFrame(gameLoop);
        }

    }




    // Collisions /////////////////////////////////////////////////////////////////////////////
    function collisionCheck(character, object) {

        var vectorX = (character.x + (character.width / 2)) - (object.x + (object.width / 2));
        var vectorY = (character.y + (character.height / 2)) - (object.y + (object.height / 2));

        var halfWidths = (character.width / 2) + (object.width / 2);
        var halfHeights = (character.height / 2) + (object.height / 2);

        var collisionDirection = null;

        if (Math.abs(vectorX) < halfWidths && Math.abs(vectorY) < halfHeights) {

            var offsetX = halfWidths - Math.abs(vectorX);
            var offsetY = halfHeights - Math.abs(vectorY);
            if (offsetX < offsetY) {

                if (vectorX > 0) {
                    collisionDirection = "left";
                    character.x += offsetX;
                }
                else {
                    collisionDirection = "right";
                    character.x -= offsetX;
                }

            }
            else {

                if (vectorY > 0) {
                    collisionDirection = "top";
                    character.y += offsetY;
                }
                else {
                    collisionDirection = "bottom";
                    character.y -= offsetY;
                }

            }

        }
        console.log(collisionDirection);
        return collisionDirection;

    }



    // clears the canvas ////////////////////////
    function clearCanvas() {
        context.clearRect(0, 0, 800, 650);
    }



    // draws a circle /////////////////////////////////////////////
    function drawCircle(width, height, radius, sAngle, eAngle) {
        context.beginPath();
        context.arc(width, height, radius, sAngle, eAngle);
        context.stroke();
    }



    // make a star ////////////////////////
    function strokeStar(x, y, r, n, inset) {
        context.save();
        context.beginPath();
        context.translate(x, y);
        context.moveTo(0, 0 - r);
        for (var i = 0; i < n; i++) {
            context.rotate(Math.PI / n);
            context.lineTo(0, 0 - (r * inset));
            context.rotate(Math.PI / n);
            context.lineTo(0, 0 - r);
        }
        context.closePath();
        context.fill();
        context.restore();
    }


    // draw the 3 stars for the flossing JammyDodger Trophies //////////////////////
    function draw3Stars() {
        context.fillStyle = "#6599FF";
        strokeStar(canvas.width / 4, 487, 30, 5, 2);
        strokeStar(canvas.width / 2, 487, 30, 5, 2);
        strokeStar(canvas.width - canvas.width / 4, 487, 30, 5, 2);
    }



    // check the players collision with the spoon enemy /////////////////////////////////////////////////////////////////////
    function checkSpoonCollisions() {

        if (collectableCount >= 0) {
            // if there is a collision ///////////////////
            if (collisionCheck(player, spoon1)) {
                spoonCollisionCount++;
                player.y += 20;
                spoon1.x = Math.floor((Math.random() * 700) + 50);
                spoon1.y = -500;
                defeatSound.play();
                targeted1 = false;
            }
            // while there is no collision //////////////////////////////////////////// 
            else {
                // draw the spoon ///////
                spoon1.draw();
                // if the spoons y position is greater than top of the canvas minus half itself (Enters the screen from top) //
                if (spoon1.y > (canvas.height - canvas.height) - (spoon1.height / 8) * 7) {

                    // if the spoon leaves the screen ////////////////
                    if (spoon1.y + spoon1.height >= canvas.height) {
                        spoon1.x = Math.floor((Math.random() * 700) + 50);
                        spoon1.y = -500;
                        // draw the spoon ///////
                        spoon1.draw();
                        targeted1 = false;
                    }

                    if (!targeted1) {

                        // if players x position plus half its width is greater than the spoons x position plus half its width //
                        if (player.x + player.width / 2 >= spoon1.x + spoon1.width / 2) {
                            // spoon moves right //
                            spoon1.x += 2;
                        }

                        // if players x position plus half its width is less than the spoons x position plus half its width //
                        if (player.x + player.width / 2 < spoon1.x + spoon1.width / 2) {
                            // spoon moves left //
                            spoon1.x -= 2;
                        }

                        if (player.x + player.width / 2 - 10 < spoon1.x + spoon1.width / 2
                            && player.x + player.width / 2 + 10 > spoon1.x + spoon1.width / 2) {
                            targeted1 = true;
                        }

                    }
                    else {
                        // spoon moves down //
                        spoon1.y += 8;
                    }

                }
                // if the spoons y position is less than or equal to the top of the canvas minus half itself // 
                else {
                    // spoon moves down //
                    spoon1.y += 3;
                }
            }
        }
        if (collectableCount >= 2) {
            // if there is a collision ///////////////////
            if (collisionCheck(player, spoon2)) {
                spoonCollisionCount++;
                player.y += 20;
                spoon2.x = Math.floor((Math.random() * 700) + 50);
                spoon2.y = -500;
                defeatSound.play();
                targeted2 = false;
            }
            // while there is no collision //////////////////////////////////////////// 
            else {
                // draw the spoon ///////
                spoon2.draw();
                // if the spoons y position is greater than top of the canvas minus half itself //
                if (spoon2.y > (canvas.height - canvas.height) - (spoon1.height / 8) * 7) {

                    // if the spoon leaves the screen ////////////////
                    if (spoon2.y + spoon2.height >= canvas.height) {
                        spoon2.x = Math.floor((Math.random() * 700) + 50);
                        spoon2.y = -500;
                        // draw the spoon ///////
                        spoon2.draw();
                        targeted2 = false;
                    }

                    if (!targeted2) {

                        // if players x position plus half its width is greater than the spoons x position plus half its width //
                        if (player.x + player.width / 2 >= spoon2.x + spoon2.width / 2) {
                            // spoon moves right //
                            spoon2.x += 2;
                        }

                        // if players x position plus half its width is less than the spoons x position plus half its width //
                        if (player.x + player.width / 2 < spoon2.x + spoon2.width / 2) {
                            // spoon moves left //
                            spoon2.x -= 2;
                        }

                        if (player.x + player.width / 2 - 10 < spoon2.x + spoon2.width / 2
                            && player.x + player.width / 2 + 10 > spoon2.x + spoon2.width / 2) {
                            targeted2 = true;
                        }

                    }
                    else {
                        // spoon moves down //
                        spoon2.y += 8;
                    }

                }
                // if the spoons y position is less than or equal to the top of the canvas minus half itself // 
                else {
                    // spoon moves down //
                    spoon2.y += 3;

                }
            }

        }
        if (collectableCount >= 3) {
            // if there is a collision ///////////////////
            if (collisionCheck(player, spoon3)) {
                spoonCollisionCount++;
                player.y += 20;
                spoon3.x = Math.floor((Math.random() * 700) + 50);
                spoon3.y = -500;
                defeatSound.play();
                targeted3 = false;
            }
            // while there is no collision //////////////////////////////////////////// 
            else {
                // draw the spoon ///////
                spoon3.draw();
                // if the spoons y position is greater than top of the canvas minus half itself //
                if (spoon3.y > (canvas.height - canvas.height) - (spoon1.height / 8) * 7) {

                    // if the spoon leaves the screen ////////////////
                    if (spoon3.y + spoon3.height >= canvas.height) {
                        spoon3.x = Math.floor((Math.random() * 700) + 50);
                        spoon3.y = -500;
                        // draw the spoon ///////
                        spoon3.draw();
                        targeted3 = false;
                    }

                    if (!targeted3) {

                        // if players x position plus half its width is greater than the spoons x position plus half its width //
                        if (player.x + player.width / 2 >= spoon3.x + spoon3.width / 2) {
                            // spoon moves right //
                            spoon3.x += 2;
                        }

                        // if players x position plus half its width is less than the spoons x position plus half its width //
                        if (player.x + player.width / 2 < spoon3.x + spoon3.width / 2) {
                            // spoon moves left //
                            spoon3.x -= 2;
                        }

                        if (player.x + player.width / 2 - 10 < spoon3.x + spoon3.width / 2
                            && player.x + player.width / 2 + 10 > spoon3.x + spoon3.width / 2) {
                            targeted3 = true;
                        }

                    }
                    else {
                        // spoon moves down //
                        spoon3.y += 8;
                    }

                }
                // if the spoons y position is less than or equal to the top of the canvas minus half itself // 
                else {
                    // spoon moves down //
                    spoon3.y += 3;

                }
            }

        }
        if (spoonCollisionCount === 3) {
            player.isAlive = false;
            gameOver();
        }

    }

    function checkCollectableCollisions() {
        if (collisionCheck(player, collectable1)) {
            collectableCount++;
            collectable1.x = -100;
            collectable1.y = -100;
            victorySound.play();
        } else {
            collectable1.draw();
        }

        if (collisionCheck(player, collectable2)) {
            collectableCount++;
            collectable2.x = -100;
            collectable2.y = -100;
            victorySound.play();
        } else {
            collectable2.draw();
        }

        if (collisionCheck(player, collectable3)) {
            collectableCount++;
            collectable3.x = -100;
            collectable3.y = -100;
            victorySound.play();
        } else {
            collectable3.draw();
        }

        if (collisionCheck(player, collectable4)) {
            collectableCount++;
            collectable4.x = -100;
            collectable4.y = -100;
            victorySound.play();
        } else {
            collectable4.draw();
        }

        if (collectableCount === 4) {
            complete();
        }
    }

    function gameOver() {

        player.isAlive = false;
        isGameOver = true;
        completed = true;

        console.log('Inside gameOver function');

        clearCanvas();

        gameOverMusic.volume = 0.8;
        if (musicOn) {
            if (theme.play() || gameOverMusic.play()) {
                theme.pause();
                theme.currentTime = 0;
                gameOverMusic.play();
            }
            if (!gameOverMusic.play()) {
                gameOverMusic.play();
            }
        }
        else {
            gameOverMusic.pause();
        }


        context.fillStyle = "#000000";
        context.fillRect(0, 0, canvas.width, canvas.height);
        context.fillStyle = "#b62e2e";
        context.font = "70px Impact";
        context.fillText('GAME OVER!!', canvas.width / 2, canvas.height / 2);
        context.fillStyle = "#03CDFF";
        context.font = "20px Arial";
        context.fillText("Press Enter to Play Again", canvas.width / 2, canvas.height - 70);


        if (!reseted) {
            requestAnimationFrame(gameOver);
        }
        else if (reseted) {
            requestAnimationFrame(reset);
        }

    }


    

    // display highscore array ///////////////////////////////////////
    function displayArray(arr) {
        context.textAlign = "center";
        context.font = "30px Impact";
        console.log(arr.length);
        for (i = 0; i < arr.length; i++) {
            console.log("inside i");
            for (j = 0; j < arr[i].length; j++) {
                console.log("inside j");
                Array.prototype.toString()
                context.fillText(arr[i][0], 320, i * 50 + 280);
                context.fillText(arr[i][1], 450, i * 50 + 280);
            }
        }
    }
};