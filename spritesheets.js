//spoon enemy spritesheet ////////////////////////////////////////////
SpoonSpritesheet = new SpriteSheet('images/SpoonSpriteSheet.png', 24, 100);
var spoonAnimation = new Animation(SpoonSpritesheet, 12, 0, 1);

//strawberry flashing spritesheet ////////////////////////////////////////////
StrawberrySpritesheet = new SpriteSheet('images/strawberrySpriteSheet.png', 64, 64);
var strawberryAnimation = new Animation(StrawberrySpritesheet, 8, 0, 7);

//Jammy Dodger animations ///////////////////////////////////////////////
JamDodgerMainSpritesheet = new SpriteSheet('images/JammyDodgerMainSpriteSheet.png', 100, 90);
JamDodgerFlexSpritesheet = new SpriteSheet('images/JammyDodgerFlexSpriteSheet.png', 109, 95);
JamDodgerFlossSpritesheet = new SpriteSheet('images/JammyDodgerFlossDance.png', 130, 100);

var runRight = new Animation(JamDodgerMainSpritesheet, 11, 4, 7);
var runLeft = new Animation(JamDodgerMainSpritesheet, 11, 8, 11);
var standingCenter = new Animation(JamDodgerFlexSpritesheet, 8, 0, 11);
var flossing = new Animation(JamDodgerFlossSpritesheet, 7, 0, 7);


// Jammy Dodger Logo animation ///////////////////////////////////////////////
spritesheetLogo = new SpriteSheet('images/JammyDodgerLogoAnimation.png', 198, 93);
var jammyDodgerLogoAnimation = new Animation(spritesheetLogo, 5, 0, 30);




// SpriteSheet function /////////////////////////////////////////////////////
function SpriteSheet(path, frameWidth, frameHeight) {
    this.image = new Image();
    this.frameWidth = frameWidth;
    this.frameHeight = frameHeight;

    // calculates the number of frames in a row after the image loads
    var self = this;
    this.image.onload = function () {
        self.framesPerRow = Math.floor(self.image.width / self.frameWidth);
    };

    this.image.src = path;
}




// Animation function ///////////////////////////////////////////////////////
function Animation(spritesheet, frameSpeed, startFrame, endFrame) {

    var animationArray = [];  // array holding the animation
    var currentFrame = 0;        // the current frame to draw
    var counter = 0;             // keeps track of frame rate

    // creates the sequence of frame numbers for the animation
    for (var frameNumber = startFrame; frameNumber <= endFrame; frameNumber++)
        animationArray.push(frameNumber);

    // Updates the animation
    this.update = function () {

        // updates to the next frame if it is time
        if (counter === (frameSpeed - 1))
            currentFrame = (currentFrame + 1) % animationArray.length;

        // updates the counter
        counter = (counter + 1) % frameSpeed;
    };

    // draw the current frame
    this.draw = function (x, y) {
        // gets the row and col of the frame
        var row = Math.floor(animationArray[currentFrame] / spritesheet.framesPerRow);
        var col = Math.floor(animationArray[currentFrame] % spritesheet.framesPerRow);

        context.drawImage(
            spritesheet.image,
            col * spritesheet.frameWidth, row * spritesheet.frameHeight,
            spritesheet.frameWidth, spritesheet.frameHeight,
            x, y,
            spritesheet.frameWidth, spritesheet.frameHeight);
    };
}